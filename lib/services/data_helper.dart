import 'dart:io';

import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DataProvider {
  static Database _database;
  DataProvider._();
  static final DataProvider db = DataProvider._();

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }
    _database = await _initDb();
    return _database;
  }

  Future<Database> _initDb() async {
    var directory = await getDatabasesPath();
    var path = join(directory, "toeic600.db");
    bool isExist = await File(path).exists();
    if (!isExist) {
      print("Creating new copy from asset");
      ByteData data = await rootBundle.load("assets/databases/toeic600.db");
      List<int> bytes =
          data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      await File(path).writeAsBytes(bytes);
    } else {
      print("Opening existing database");
    }
    return await openDatabase(path);
  }
}
