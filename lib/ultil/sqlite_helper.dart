import 'package:toeic_600/services/data_helper.dart';
import 'package:toeic_600/model/essential.dart';
import 'package:toeic_600/model/topic.dart';

class SqliteHelper {
  static final String essentialTable = 'ESSENTIAL';
  static final String topicTable = 'TOPIC';

  Future<List<Essential>> getEssential() async {
    final db = await DataProvider.db.database;
    var res = await db.query(essentialTable);
    print('SqlHelper:' + res.toString());
    List<Essential> essentials =
        res.isNotEmpty ? res.map((e) => Essential.fromMap(e)).toList() : [];
    print('SqlHelper: mapping essential success');
    return essentials;
  }

  Future<List<Topic>> getTopic() async {
    final db = await DataProvider.db.database;
    var res = await db.query(topicTable);
    print('SqlHelper:' + res.toString());
    List<Topic> topics =
        res.isNotEmpty ? res.map((e) => Topic.fromMap(e)).toList() : [];

    print('SqlHelper: mapping topic success');
    return topics;
  }

  Future<List<Essential>> getDetailTopic(int topic) async {
    final db = await DataProvider.db.database;
    var res =
        await db.query(essentialTable, where: "topic = ? ", whereArgs: [topic]);
    print('SqlHelper topic:' + res.toString());
    List<Essential> essentials =
        res.isNotEmpty ? res.map((e) => Essential.fromMap(e)).toList() : [];
    print('SqlHelper: mapping essential successssssssssssssss');
    return essentials;
  }
}
