import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();
  @override
  List<Object> get props => [];
}

class ViewDetail extends HomeEvent {
  final int topicID;
  const ViewDetail({@required this.topicID});
}

class LoadData extends HomeEvent {
  const LoadData();
}
