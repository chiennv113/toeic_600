import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:toeic_600/model/essential.dart';
import 'package:toeic_600/model/topic.dart';

abstract class HomeState extends Equatable {
  const HomeState();
}

class HomeLoading extends HomeState {
  const HomeLoading();
  @override
  List<Object> get props => [];
}

class HomeLoaded extends HomeState {
  final List<Topic> topics;
  final List<Essential> essentials;
  const HomeLoaded({@required this.topics, this.essentials});
  @override
  List<Object> get props => [topics, essentials];
}

class HomeError extends HomeState {
  final String error;
  const HomeError({@required this.error});
  @override
  List<Object> get props => [error];
}


