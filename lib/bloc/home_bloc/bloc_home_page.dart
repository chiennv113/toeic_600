import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:toeic_600/model/essential.dart';
import 'package:toeic_600/model/topic.dart';
import 'package:toeic_600/ultil/sqlite_helper.dart';

import 'home_event.dart';
import 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final SqliteHelper helper = SqliteHelper();
  @override
  HomeState get initialState {
    return HomeLoading();
  }

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    switch (event.runtimeType) {
      // case ViewDetail:
      //   yield* _mapToViewDetailEvent(event);
      //   break;
      default:
        yield* _mapToLoadEvent(event);
        break;
    }
  }

  // Stream<DetailState> _mapToViewDetailEvent(ViewDetail event) async* {
  //   yield DetailLoading();
  //   try {
  //     int topicID;
  //     List<Essential> essentials = await helper.getDetailTopic(topicID);
  //     if (essentials.isNotEmpty) {
  //       yield DetailLoaded(essentials: essentials);
  //     }
  //   } catch (_) {
  //     yield DetailError(error: 'ERROR_DATA_NOT_EXIST');
  //   }
  // }

  Stream<HomeState> _mapToLoadEvent(LoadData event) async* {
    yield HomeLoading();
    try {
      print('----------------start load data');
      List<Topic> topics = await helper.getTopic();
      List<Essential> essentials = await helper.getEssential();
      if (topics.isNotEmpty && essentials.isNotEmpty) {
        yield HomeLoaded(topics: topics, essentials: essentials);
        print('-----------------Load success');
      }
    } catch (_) {
      yield HomeError(error: 'ERROR_DATA_NOT_EXIST');
    }
  }
}
