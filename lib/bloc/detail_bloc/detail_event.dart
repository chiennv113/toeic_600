import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class DetailEvent extends Equatable {
  const DetailEvent();
  @override
  List<Object> get props => [];
}

class LoadDataTopic extends DetailEvent {
  final int topicID;
  const LoadDataTopic({@required this.topicID});
}
