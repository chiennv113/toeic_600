import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:toeic_600/model/essential.dart';


abstract class DetailState extends Equatable {
  const DetailState();
}

class DetailLoading extends DetailState {
  const DetailLoading();
  @override
  List<Object> get props => [];
}

class DetailLoaded extends DetailState {
  final List<Essential> essentials;
  const DetailLoaded({@required this.essentials});
  @override
  List<Object> get props => [essentials];
}

class DetailError extends DetailState {
  final String error;
  const DetailError({@required this.error});
  @override
  List<Object> get props => [error];
}
