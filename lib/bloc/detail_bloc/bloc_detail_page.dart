import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:toeic_600/bloc/detail_bloc/detail_state.dart';
import 'package:toeic_600/model/essential.dart';
import 'package:toeic_600/ultil/sqlite_helper.dart';

import 'detail_event.dart';

class DetailBloc extends Bloc<DetailEvent, DetailState> {
  final SqliteHelper helper = SqliteHelper();
  @override
  DetailState get initialState => DetailLoading();

  @override
  Stream<DetailState> mapEventToState(DetailEvent event) async* {
    switch (event.runtimeType) {
      default:
        yield* _mapToLoadEvent(event);
        break;
    }
  }

  Stream<DetailState> _mapToLoadEvent(LoadDataTopic event) async* {
    yield DetailLoading();
    try {
      print('----------------start load data');
      List<Essential> essentials = await helper.getDetailTopic(event.topicID);
      if (essentials.isNotEmpty) {
        yield DetailLoaded(essentials: essentials);
        print('-----------------Load success---------');
      }
    } catch (_) {
      yield DetailError(error: 'ERROR_DATA_NOT_EXIST');
    }
  }
}
