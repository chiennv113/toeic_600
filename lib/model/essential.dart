class Essential {
  int id;
  int topic;
  String vocabulary;
  String vocalization;
  String explanation;
  String kind;
  String translate;
  String example;
  String exTranslate;
  int favourite;

  Essential({
    this.id,
    this.topic,
    this.vocabulary,
    this.vocalization,
    this.explanation,
    this.kind,
    this.translate,
    this.example,
    this.exTranslate,
    this.favourite,
  });
  factory Essential.fromMap(Map<String, dynamic> json) => new Essential(
        id: json["id"],
        topic: json["topic"],
        vocabulary: json["vocabulary"],
        vocalization: json["vocalization"],
        explanation: json["explanation"],
        kind: json["kind"],
        translate: json["translate"],
        example: json["example"],
        exTranslate: json["ex_translate"],
        favourite: json["favourite"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "topic": topic,
        "vocabulary": vocabulary,
        "vocalization": vocalization,
        "explanation": explanation,
        "kind": kind,
        "translate": translate,
        "example": example,
        "ex_translate": exTranslate,
        "favourite": favourite,
      };
}
