class Topic{
  int id;
  String name;
  String translate;

  Topic({this.id, this.name, this.translate});
  factory Topic.fromMap(Map<String, dynamic> json) => new Topic(
        id: json["id"],
        name: json["name"],
        translate: json["translate"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "translate": translate,
      };
}
