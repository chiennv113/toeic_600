import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:toeic_600/bloc/detail_bloc/bloc_detail_page.dart';
import 'package:toeic_600/bloc/detail_bloc/detail_event.dart';
import 'package:toeic_600/bloc/detail_bloc/detail_state.dart';
import 'package:toeic_600/bloc/home_bloc/home_event.dart';
import 'package:toeic_600/model/essential.dart';
import 'package:toeic_600/view_model/detail_vm.dart';

class ItemTopicPage extends StatefulWidget {
  @override
  _ItemTopicPageState createState() => _ItemTopicPageState();
  final int idTopic;
  final String title;
  ItemTopicPage({Key key, @required this.idTopic, @required this.title})
      : super(key: key);
}

class _ItemTopicPageState extends State<ItemTopicPage> {
  final DetailBloc _detailBloc = DetailBloc();

  @override
  void initState() {
    super.initState();
    _detailBloc.add(LoadDataTopic(topicID: widget.idTopic));
    print(widget.idTopic.toString());
    print(widget.title.toString());
  }

  Widget item(Essential essential) {
    return Card(
      elevation: 10,
      child: Container(
        margin: EdgeInsets.all(10),
        height: 150,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Text(
                          essential.vocabulary,
                          style: TextStyle(color: Colors.green),
                        ),
                      ),
                      Text(
                        essential.vocalization,
                        style: TextStyle(color: Colors.red),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Text(
                      essential.explanation,
                      style: TextStyle(color: Colors.blue),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Row(
                      children: <Widget>[
                        Text(essential.kind,
                            style: TextStyle(color: Colors.black54)),
                        Expanded(
                          child: Text(essential.translate,
                              style: TextStyle(color: Colors.black54)),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Text(essential.example),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        elevation: 10,
        title: Text(
          'Vocabulary ${widget.title}',
          style: TextStyle(color: Colors.red),
        ),
      ),
      body: BlocProvider<DetailBloc>(
        create: (BuildContext context) {
          return _detailBloc;
        },
        child: BlocBuilder<DetailBloc, DetailState>(
          bloc: _detailBloc,
          builder: (context, state) {
            if (state is DetailLoading) {
              return Center(
                child: SizedBox(
                  child: CircularProgressIndicator(),
                ),
              );
            } else if (state is DetailLoaded) {
              return ListView.builder(
                  itemCount:
                      state.essentials != null ? state.essentials.length : 0,
                  itemBuilder: (context, index) =>
                      item(state.essentials[index]));
            } else {
              return Center(
                child: Text('Error'),
              );
            }
          },
        ),
      ),
    );
  }
}
