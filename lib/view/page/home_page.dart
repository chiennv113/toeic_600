import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:toeic_600/bloc/home_bloc/bloc_home_page.dart';
import 'package:toeic_600/bloc/home_bloc/home_event.dart';
import 'package:toeic_600/bloc/home_bloc/home_state.dart';
import 'package:toeic_600/images/images.dart';
import 'package:toeic_600/model/essential.dart';
import 'package:toeic_600/model/topic.dart';
import 'package:toeic_600/view/widget/sliver_app_bar_delegate.dart';

import 'detail_topic_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  final HomeBloc _homeBloc = HomeBloc();
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _homeBloc.add(LoadData());
    _tabController = new TabController(length: 2, vsync: this);
  }

  Widget _itemTopic(Topic topic) {
    return Scaffold(
      body: Card(
        child: Container(
          margin: EdgeInsets.all(10),
          width: MediaQuery.of(context).size.width / 2,
          height: MediaQuery.of(context).size.width / 2,
          child: Stack(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                  image: NetworkImage(IMG_TOPIC),
                  fit: BoxFit.cover,
                )),
              ),
              Positioned(
                  bottom: 0,
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2,
                    padding: EdgeInsets.only(left: 10),
                    alignment: Alignment.centerLeft,
                    height: MediaQuery.of(context).size.width / 2 / 5,
                    color: Colors.black38,
                    child: Text(
                      topic != null ? topic.name : "",
                      style: TextStyle(color: Colors.white),
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }

  Widget itemVocabulary(Essential essential) {
    return Card(
      elevation: 10,
      child: Container(
        margin: EdgeInsets.all(10),
        height: 150,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Text(
                          essential.vocabulary,
                          style: TextStyle(color: Colors.green),
                        ),
                      ),
                      Text(
                        essential.vocalization,
                        style: TextStyle(color: Colors.red),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Text(
                      essential.explanation,
                      style: TextStyle(color: Colors.blue),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Row(
                      children: <Widget>[
                        Text(essential.kind,
                            style: TextStyle(color: Colors.black54)),
                        Expanded(
                          child: Text(essential.translate,
                              style: TextStyle(color: Colors.black54)),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Text(essential.example),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<HomeBloc>(
      create: (BuildContext context) {
        return _homeBloc;
      },
      child: Scaffold(
        body: DefaultTabController(
          length: 2,
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  backgroundColor: Colors.white,
                  expandedHeight: 100,
                  pinned: true,
                  elevation: 0,
                  flexibleSpace: FlexibleSpaceBar(
                    centerTitle: false,
                    title: Text(
                      "Learn 600 Toeic",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                          fontFamily: 'Avenir'),
                    ),
                  ),
                ),
                SliverPersistentHeader(
                  pinned: true,
                  delegate: SliverAppBarDelegate(
                    TabBar(
                      controller: _tabController,
                      labelStyle: TextStyle(fontWeight: FontWeight.w500),
                      labelColor: Colors.black,
                      unselectedLabelColor: Colors.grey,
                      indicator: UnderlineTabIndicator(
                        borderSide: BorderSide(width: 0, color: Colors.white),
                        insets: EdgeInsets.symmetric(horizontal: 20),
                      ),
                      tabs: [
                        Tab(text: "Topic"),
                        Tab(text: "Vocabulary"),
                      ],
                    ),
                  ),
                ),
              ];
            },
            body: TabBarView(
              controller: _tabController,
              children: <Widget>[
                BlocBuilder<HomeBloc, HomeState>(
                    bloc: _homeBloc,
                    builder: (context, state) {
                      if (state is HomeLoading) {
                        return Center(
                          child: SizedBox(
                            child: CircularProgressIndicator(),
                          ),
                        );
                      } else if (state is HomeLoaded) {
                        return GridView.builder(
                            itemCount:
                                state.topics != null ? state.topics.length : 0,
                            shrinkWrap: true,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2),
                            itemBuilder: (context, index) => InkWell(
                                  child: _itemTopic(state.topics[index]),
                                  onTap: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => ItemTopicPage(
                                                idTopic: state.topics[index].id,
                                                title: state.topics[index].name
                                                    .toString(),
                                              ))),
                                ));
                      } else {
                        return Center(
                          child: Text('Error'),
                        );
                      }
                    }),
                BlocBuilder<HomeBloc, HomeState>(
                  bloc: _homeBloc,
                  builder: (context, state) {
                    if (state is HomeLoading) {
                      return Center(
                        child: SizedBox(
                          child: CircularProgressIndicator(),
                        ),
                      );
                    } else if (state is HomeLoaded) {
                      return ListView.builder(
                          itemCount: state.essentials != null
                              ? state.essentials.length
                              : 0,
                          itemBuilder: (context, index) =>
                              itemVocabulary(state.essentials[index]));
                    } else {
                      return Center(
                        child: Text('Error'),
                      );
                    }
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
