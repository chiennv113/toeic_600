import 'package:flutter/cupertino.dart';
import 'package:toeic_600/model/essential.dart';
import 'package:toeic_600/model/topic.dart';
import 'package:toeic_600/ultil/sqlite_helper.dart';

class HomePageVM extends ChangeNotifier {
  var helper = new SqliteHelper();
  List<Essential> essentials;
  List<Topic> topics;
  HomePageVM() {
    setDbTopic();
    setDbAssential();
  }
  void setDbAssential() async {
    essentials = await helper.getEssential();
    notifyListeners();
  }

  void setDbTopic() async {
    topics = await helper.getTopic();
    notifyListeners();
  }
}
