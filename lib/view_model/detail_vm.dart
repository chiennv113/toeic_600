import 'package:flutter/foundation.dart';
import 'package:toeic_600/model/essential.dart';
import 'package:toeic_600/ultil/sqlite_helper.dart';

class DetailPageVM extends ChangeNotifier {
  var helper = new SqliteHelper();
  List<Essential> essentials;
  DetailPageVM(int topic) {
    setDatailTopic(topic);
  }
  void setDatailTopic(int topic) async {
    essentials = await helper.getDetailTopic(topic);
    notifyListeners();
  }
}
